from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.contrib.auth import authenticate, login, logout
from django.core.urlresolvers import reverse
from django.template import RequestContext

import PIL
from matplotlib import pylab
from pylab import *
import StringIO

from algorithms.utils import evaluate_algorithm

def best_fitness(request):
    r = evaluate_algorithm(100, 4, 0.05, 'genetic', 'ackley', 50, ['best_fitness'])
    plot(r[0], linewidth=2)

    xlabel = 'Generations'
    ylabel = 'Fitness'
    title('Best Fitness per Generation')
    grid(True)

    buffer = StringIO.StringIO()
    canvas = pylab.get_current_fig_manager().canvas
    canvas.draw()
    graphIMG = PIL.Image.frombytes("RGB", canvas.get_width_height(), canvas.tostring_rgb())
    graphIMG.save(buffer, "PNG")
    pylab.close()

    output = buffer.getvalue().encode("base64")
    buffer.close()

    return HttpResponse('<img src="data:image/png;base64,' + output + ' />')
    # return HttpResponse(buffer.getvalue() , content_type='image/png')


# Create your views here.
def login_user(request):
    context = RequestContext(request)
    return render(request, 'login.html', context)


def authenticate_user(request):
    context = RequestContext(request)
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)

    if user is not None:
        if user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
        else:
            # Return a 'disabled account' error message
            context['disabled_account'] = True
            return render(request, 'login.html', context)
    else:
        # Return an 'invalid login' error message.
        context['invalid_login'] = True
        return render(request, 'login.html', context)


def logout_user(request):
    context = RequestContext(request)
    logout(request)
    return render(request, 'login.html', context)


@login_required
def index(request):
    context = {
        'page_title':'Home',
    }

    return render(request, 'index.html', context)

@login_required
def notifications(request):
    context = {
        'page_title':'Home',
        'ntf_groups':['Home', 'Company'],
    }

    return render(request, 'notifications.html', context)

@login_required
def cameras(request):
    context = {
        'page_title':'Cameras',
        'cmr_groups':['Home', 'Company'],
    }

    return render(request, 'cameras.html', context)

@login_required
def users(request):
    context = {
        'page_title':'Users',
        'usr_groups':['Home', 'Company'],
    }

    return render(request, 'users.html', context)
