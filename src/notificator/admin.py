from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(CameraProfile)

admin.site.register(UserProfile)

admin.site.register(NotificationLog)
