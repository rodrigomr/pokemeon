from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_POST
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate, login
from notificator.models import *
from django.template import RequestContext, loader
import time
import os


PICTURE_FILE = 0
VIDEO_FILE = 1

def index(request):
    template = loader.get_template('notificator/index.html')
    context = RequestContext(request)

    return HttpResponse(template.render(context))


@csrf_exempt
@require_POST
def notify_moviment(request):
    camera = authenticate_camera(request)
    if camera is not None:
        camera.userprofile_set.all().update(notify=True)
        notification = NotificationLog(camera=camera)
        notification.save()

        return HttpResponse("SUCCESS")

    else:
        return HttpResponse("INVALID_CAMERA")


@csrf_exempt
@require_POST
def receive_picture(request):
    camera = authenticate_camera(request)
    if camera is not None:
        picture_path = handle_uploaded_file(request.FILES['picture'], PICTURE_FILE, camera.serial.username)

        notification = NotificationLog.objects.filter(camera=camera).latest('timestamp')
        notification.picture_url = picture_path
        notification.save()

        return HttpResponse("SUCCESS")

    else:
         return HttpResponse("INVALID_CAMERA")


@csrf_exempt
@require_POST
def receive_video(request):
    camera = authenticate_camera(request)
    if camera is not None:
        video_path = handle_uploaded_file(request.FILES['video'], VIDEO_FILE, camera.serial.username)

        notification = NotificationLog.objects.filter(camera=camera).latest('timestamp')
        notification.video_url = video_path
        notification.save()

        return HttpResponse("SUCCESS")

    else:
         return HttpResponse("INVALID_CAMERA")


def test_media(request): # APENAS TESTE! BATMAN IRA APAGAR DEPOIS
    # pega a ultima notificacao da camera 1 e exibe
    template = loader.get_template('notificator/test_media.html')
    context = RequestContext(request)

    camera = CameraProfile.objects.get(pk=1)
    notification = NotificationLog.objects.filter(camera=camera).latest('timestamp')
    context['notification'] = notification

    return HttpResponse(template.render(context))


####### AUXILIARY FUNCTIONS ##############
def authenticate_camera(request):
    if request.POST.has_key("serialnumber") and request.POST.has_key("password"):
        serialNumber = request.POST["serialnumber"]
        password = request.POST["password"]

        # Authenticate and set DB info
        user = authenticate(username=serialNumber, password=password)
        if user is not None and user.is_active:
                # Setting DB flag, informing that was movement
                camera = CameraProfile.objects.get(serial=user)

                return camera

    # Error authenticating
    return None

def handle_uploaded_file(up_file, file_type, camera_name):
    currentTime = str(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
    static_path = "notificator/static/"
    file_path = "notificator/"
    if file_type == PICTURE_FILE:
        file_path += "pictures/" + camera_name + "/"
        file_name = currentTime + '.jpg'
    elif file_type == VIDEO_FILE:
        file_path += "videos/" + camera_name + "/"
        file_name = currentTime + '.mp4'

    if not os.path.exists(static_path + file_path):
        os.makedirs(static_path + file_path)

    destination = open(static_path + file_path + file_name, 'wb+')
    for chunk in up_file.chunks():
        destination.write(chunk)
    destination.close()

    return file_path + file_name
