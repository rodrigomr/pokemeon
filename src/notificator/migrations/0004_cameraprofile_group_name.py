# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notificator', '0003_auto_20151022_0002'),
    ]

    operations = [
        migrations.AddField(
            model_name='cameraprofile',
            name='group_name',
            field=models.CharField(default='migration_group', max_length=30),
            preserve_default=False,
        ),
    ]
