# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notificator', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='notificationlog',
            name='picture_url',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='notificationlog',
            name='video_url',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
