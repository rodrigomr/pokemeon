# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notificator', '0002_auto_20151021_2203'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificationlog',
            name='camera',
            field=models.ForeignKey(to='notificator.CameraProfile'),
        ),
    ]
