from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class CameraProfile(models.Model):
    latitude  = models.FloatField()
    longitude = models.FloatField()
    serial    = models.OneToOneField(User)
    group_name = models.CharField(max_length=30)

    def __str__(self):
        return "Camera number %d" %self.id

class UserProfile(models.Model):
    user    = models.OneToOneField(User)
    cameras = models.ManyToManyField(CameraProfile)
    notify  = models.BooleanField(default=False)

    def __str__(self):
        return "%s's profile" %self.user

class NotificationLog(models.Model):
    camera = models.ForeignKey(CameraProfile)
    timestamp = models.DateTimeField(auto_now_add=True)
    picture_url = models.CharField(max_length=255, blank=True, null=True)
    video_url = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return ("Notificaton of camera %d at " %self.camera.id) + \
               str(self.timestamp)



