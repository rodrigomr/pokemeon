from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^notifymoviment', views.notify_moviment, name='notify_moviment'),
    url(r'^receivepicture', views.receive_picture, name='receive_picture'),
    url(r'^receivevideo', views.receive_video, name='receive_video'),
    url(r'^testmedia', views.test_media, name='test_media'), # APENAS TESTE!  APAGAR DEPOIS
]
