"""pokemeon URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^notificator/', include('notificator.urls')),

      #projects
    url(r'^evolutives_report/best_fitness.png/$', 'evolreport.views.best_fitness', name='graph'),
    url(r'^$', 'evolreport.views.index', name='index'),
    url(r'^notifications$', 'evolreport.views.notifications', name='cameras'),
    url(r'^cameras$', 'evolreport.views.cameras', name='cameras'),
    url(r'^users$', 'evolreport.views.users', name='users'),
    url(r'^accounts/login/', 'evolreport.views.login_user', name='login'),
    url(r'^accounts/authenticate/', 'evolreport.views.authenticate_user', name='authenticate_user'),
    url(r'^accounts/logout/', 'evolreport.views.logout_user', name='logout_user'),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
