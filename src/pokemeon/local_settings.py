# Used to integrate the database with the python db
# without need to change anything in the settings
#
#To use the local_settings type:
# $ python manage.py "comand_name" --settings=pokemeon.local_settings

from settings import *

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.postgresql_psycopg2',
#         'NAME': 'Pokemeon',
#         'USER': 'postgres',
#         'PASSWORD': 'postgres',
#         'HOST': 'localhost',
#     }
# }

#Python DB
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
