PokeMeON - Poke Me on Movement
================================

Trata-se de um sistema de monitoramento de movimento baseado em processamento de vídeo em tempo real, sendo capaz de detectar alterações no ambiente via uma câmera de segurança.
O sistema utiliza uma câmera conectada a um sistema computacional, este processa constantemente as imagens fornecidas pela câmera e é capaz de detectar movimento no ambiente. Além disso, no momento em o movimento é detectado, os usuários são notificados via uma interface web, ou ainda um aplicativo para Android, sendo capaz de visualizar tal fato via streaming.

- - -

# Membros

Henrique Sueno Nishi                 7546968  
Lucas Carvalho Gomes                 7145018  
Raphael Victor Ferreira              7143889  
Rodrigo Martins Racanicci            7546972  
Victor Lopes de Castro Martinelli    7546822  


# Requisitos

* Conexão com a internet;
* Câmera digital;
* Servidor central;
* Sistema computacional controlador para interface com a câmera (computador pessoal, Raspberry Pi, Galileo, etc);
* Celular Android ou máquina com conexão à internet bem como browser atualizado.


# Funcionalidades

* Processamento de vídeo para detecção de movimento;
* Notificações em tempo real;
* Streaming do vídeo que contém movimento;
* Streaming do vídeo a qualquer momento requisitado pelo usuário;
* Armazenamento do vídeo;
* Possibilidade de vários usuários monitorarem o mesmo sistema.


# Interfaces

Basicamente existe um controlador que capta as imagens fornecidas pela câmera e processa as mesmas. Este se comunica com um servidor central, capaz de notificar usuários através de uma aplicação Web ou aplicativo Android.

![Interfaces](/project/interfaces.png)

# Mindmap

![Mindmap](/project/mindmap.png)

# Tecnologias

* Camera System
	+ Camera digital;
	+ Python – linguagem;
	+ OpenCV – processamento de imagens;
	+ Flask – streaming de vídeo;
* Central Server
	+ Heroku - Django;
* Users
	+ Pagina Web – Django;
	+ Android App - Java;


# Cronograma

![Timetable](/project/timetable.png)
