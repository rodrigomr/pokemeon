# TODO:
# - incorporate this code in the motion detector (make a class or something)
# - save the response text in an html when an error occurred

import requests

SERVER_IP   = 'localhost'
SERVER_PORT = 8000

def check_response(response, function_name):
    if response.text != "SUCCESS" and response.text != "INVALID_CAMERA":
        open('error_'  + function_name + '.html', 'w').write(response.text)
        print function_name + ": an error ocurred, check the html error file."
    else:
        print function_name + ": " + response.text


def notify_server(camera_login):
    response = requests.post('http://%s:%d/notificator/notifymoviment' \
                             %(SERVER_IP, SERVER_PORT), data=camera_login)
    check_response(response, "notify_server")


def send_picture(camera_login, picture_url):
    response = requests.post('http://%s:%d/notificator/receivepicture' \
                             %(SERVER_IP, SERVER_PORT), data=camera_login, files={'picture': open(picture_url, 'rb')})
    check_response(response, "send_picture")


def send_video(camera_login, video_url):
    response = requests.post('http://%s:%d/notificator/receivevideo' \
                             %(SERVER_IP, SERVER_PORT), data=camera_login, files={'video': open(video_url, 'rb')})
    check_response(response, "send_video")


if __name__ == "__main__":
    camera_login = {"serialnumber": "camera1", "password": "camera1"}
    picture_url = 'teste.jpg'
    video_url = 'teste.mp4'

    notify_server(camera_login)
    send_picture(camera_login, picture_url)
    send_video(camera_login, video_url)
