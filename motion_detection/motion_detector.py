#   TODO:
#   - intall ffmpeg 2.7.2?
#  - improve motion detection algorithm?
#   - rename variables: underscores?
#   - DEBUG/OFFLINE MODE for function notify server/video release


import datetime
import time
import cv2
import requests
import sys


SERVER_IP = '192.168.1.105'
SERVER_PORT = 8000


def notify_server():
    response = requests.post('http://%s:%d/notificator/' % (SERVER_IP, SERVER_PORT))
    print response.text


def detect_movement(refrenceFrame, lastFrame):
    occupied = False
    # compute the absolute difference between the current frame and first frame
    frameDelta = cv2.absdiff(refrenceFrame, lastFrame)
    thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]

    # dilate the thresholded image to fill in holes, then find contours on thresholded image
    thresh = cv2.dilate(thresh, None, iterations=2)
    (imgNO, cnts, hieNO) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
                                            cv2.CHAIN_APPROX_SIMPLE)

    # loop over the contours
    for c in cnts:
        # if the contour is too small, ignore it
        if cv2.contourArea(c) < 500+1:
            continue

        # compute the bounding box for the contour, draw it on the frame, and update the text
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
        occupied = True

    return occupied, thresh, frameDelta


def get_current_frame(camera):
    # grab the current frame and initialize the occupied/unoccupied text
        (grabbed, frame) = camera.read()

        # if the frame could not be grabbed, then we have reached the end of the video
        if not grabbed:
            return (None, None)

        # resize the frame, convert it to grayscale, and blur it
        frame = cv2.resize(frame, (500, 375))
        currentFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        currentFrame = cv2.GaussianBlur(currentFrame, (21, 21), 0)

        return (frame, currentFrame)


def clean_up_and_exit():
    print "ERROR reading the camera"
    camera.release()
    cv2.destroyAllWindows()
    sys.exit()


def show_frames(frame, thresh, frameDelta, occupied):
    text = "Occupied" if occupied else "Unoccupied"
    # draw the text and timestamp on the frame
    cv2.putText(frame, "Room Status: {}".format(text), (10, 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)

    # show the frame and record until the user presses a key
    cv2.imshow("Security Feed", frame)
    cv2.imshow("Thresh", thresh)
    cv2.imshow("Frame Delta", frameDelta)


def update_referece_frame(start_time, pastFrame, referenceFrame):
    # detect if anything changed in the room, and adapts to it
    if(time.time() - start_time > 5):
        (changed, unused1, unused2) = detect_movement(currentFrame, pastFrame)
        pastFrame = currentFrame
        start_time = time.time()
        if not changed:
            referenceFrame = currentFrame

    return (start_time, pastFrame, referenceFrame)


def prepare_notification(occupied, canNotify, frame, video):
    if (occupied and canNotify):
        # notify_server()
        currentTime = str(time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime()))
        fourcc = cv2.VideoWriter_fourcc(*'X264')
        video = cv2.VideoWriter(currentTime + '.mp4', fourcc, 20.0, (500, 375))
        canNotify = False
        print "PREPARE VIDEO"

    elif(occupied):
        video.write(frame)

    elif (not occupied and not canNotify):
        video.release()
        canNotify = True
        print "RELEASE"

    return (canNotify, frame, video)


if __name__ == '__main__':
    camera = cv2.VideoCapture(0)
    time.sleep(0.25)

    # initialize the parameters of the video stream
    canNotify = True
    video = None
    start_time = 0

    (frame, currentFrame) = get_current_frame(camera)
    if (currentFrame is None):
        clean_up_and_exit()

    pastFrame = referenceFrame = currentFrame

    while True:
        # grab the current frame
        (frame, currentFrame) = get_current_frame(camera)
        if (currentFrame is None):
            clean_up_and_exit()

        # detect motion
        occupied, thresh, frameDelta = detect_movement(referenceFrame, currentFrame)

        canNotify, frame, video = prepare_notification(occupied, canNotify, frame, video)

        # if not changed in the past seconds update the reference frame
        start_time, pastFrame, referenceFrame = update_referece_frame(start_time, pastFrame,
                                                                      referenceFrame)

        show_frames(frame, thresh, frameDelta, occupied)

        # if the `q` key is pressed, break from the lop
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            break

    # cleanup the camera and close any open windows
    camera.release()
    if video is not None:
        video.release()
    cv2.destroyAllWindows()
